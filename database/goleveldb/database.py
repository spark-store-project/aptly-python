
from database.goleveldb import db
from database.goleveldb.storage import *
from sqlalchemy.orm import scoped_session
def internalOpen(path:str, throttleCompaction:bool)->scoped_session:
	
	return db.getSession(path)

# // NewDB creates new instance of DB, but doesn't open it (yet)
def NewDB(path :str) ->Storage:
	storage=Storage()
	storage.path=path
	storage.db=db.getSession(path)
	return storage


# // NewOpenDB creates new instance of DB and opens it
def NewOpenDB(path :str)  :
	db = NewDB(path)
	

	return db, db.Open()


# // RecoverDB recovers LevelDB database from corruption
# def RecoverDB(path string) error {
# 	stor, err := leveldbstorage.OpenFile(path, false)
# 	if err is not None  {
# 		return err
# 	}

# 	db, err := leveldb.Recover(stor, None)
# 	if err is not None  {
# 		return err
# 	}

# 	db.Close()
# 	stor.Close()

# 	return None
# }
