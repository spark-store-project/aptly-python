from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker,scoped_session
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,BLOB,Text,Integer
from sqlalchemy.pool import SingletonThreadPool

Base=declarative_base()
metadata=Base.metadata

class packageList(Base):
    __tablename__='package'
    __table_args__={'comment':'软件包管理','sqlite_autoincrement':True}
    id=Column(Integer(),primary_key=True)
    key=Column(Text(),comment='')
    value=Column(BLOB(),comment='')
def getSession(path:str):
    engine=create_engine('sqlite+pysqlite:///{}'.format(path),echo=False,
    poolclass=SingletonThreadPool,
    connect_args={'check_same_thread':False})
    session=scoped_session(sessionmaker(autocommit=True,autoflush=True,bind=engine))
    Base.metadata.create_all(engine,checkfirst=True)
    return session