# // Package pgp provides interface to signature generation and validation
# package pgp

# import (
# 	"fmt"
# 	"io"
# 	"os"
# )
import os
from typing import List,Dict,Tuple
# // Key is key in PGP representation
class Key(str):

	# // Matches checks two keys for equality
	def  Matches(key1,key2 ) ->bool :
		if key1 == key2 :
			return True
		

		if len(key1) == 8 and len(key2) == 16 :
			return key1 == key2[8:]
		

		if len(key1) == 16 and len(key2) == 8 :
			return key1[8:] == key2
		

		return False


# // KeyFromUint64 converts openpgp uint64 into hex human-readable
def KeyFromUint64(key :int) :
	return Key(hex(key))


# // KeyInfo is response from signature verification
# type KeyInfo struct {
# 	GoodKeys    []Key
# 	MissingKeys []Key
# }
class KeyInfo:
	GoodKeys :List[str]
	MissingKeys :List[str]


# // Signer interface describes facility implementing signing of files
# type Signer interface {
# 	Init() error
# 	SetKey(keyRef string)
# 	SetKeyRing(keyring, secretKeyring string)
# 	SetPassphrase(passphrase, passphraseFile string)
# 	SetBatch(batch bool)
# 	DetachedSign(source string, destination string) error
# 	ClearSign(source string, destination string) error
# }

# def Signer:
# 	def Init()
# 	def SetKey(keyRef:str)
# 	def SetKeyRing(keyring, secretKeyring:str)
# 	def SetPassphrase(passphrase, passphraseFile:str)
# 	def SetBatch(batch:bool)
# 	def DetachedSign(source:str, destination:str)
# 	def ClearSign(source:str, destination:str)
	
# // Verifier interface describes signature verification factility
# type Verifier interface {
# 	InitKeyring() error
# 	AddKeyring(keyring string)
# 	VerifyDetachedSignature(signature, cleartext io.Reader, showKeyTip bool) error
# 	IsClearSigned(clearsigned io.Reader) (bool, error)
# 	VerifyClearsigned(clearsigned io.Reader, showKeyTip bool) (*KeyInfo, error)
# 	ExtractClearsigned(clearsigned io.Reader) (text *os.File, err error)
# }

# def Verifier:
# 	def InitKeyring()
# 	def AddKeyring(keyring:str)
# 	def VerifyDetachedSignature(signature, cleartext, showKeyTip:bool)
# 	def IsClearSigned(clearsigned)
# 	def VerifyClearsigned(clearsigned, showKeyTip:bool)
# 	def ExtractClearsigned(clearsigned)