# package pgp

# import (
# 	"bytes"
# 	"crypto"
# 	"crypto/dsa" //nolint:staticcheck
# 	"crypto/ecdsa"
# 	"crypto/rsa"
# 	"hash"
# 	"io"
# 	"strconv"
# 	"time"

# 	"github.com/ProtonMail/go-crypto/openpgp"
# 	"github.com/ProtonMail/go-crypto/openpgp/armor"
# 	"github.com/ProtonMail/go-crypto/openpgp/errors"
# 	"github.com/ProtonMail/go-crypto/openpgp/packet"
# )

# // hashForSignature returns a pair of hashes that can be used to verify a
# // signature. The signature may specify that the contents of the signed message
# // should be preprocessed (i.e. to normalize line endings). Thus this function
# // returns two hashes. The second should be used to hash the message itself and
# // performs any needed preprocessing.
def hashForSignature(hashID crypto.Hash, sigType packet.SignatureType) (hash.Hash, hash.Hash, error) :
	if not hashID.Available() :
		return   None ,   None , errors.UnsupportedError("hash not available: " + strconv.Itoa(int(hashID)))
	
	h = hashID.New()

	switch sigType {
	case packet.SigTypeBinary:
		return h, h,   None 
	case packet.SigTypeText:
		return h, openpgp.NewCanonicalTextHash(h),   None 
	}

	return   None ,   None , errors.UnsupportedError("unsupported signature type: " + strconv.Itoa(int(sigType)))


class signatureResult  :
	CreationTime =None
	IssuerKeyID:int  =None
	PubKeyAlgo   =None#packet.PublicKeyAlgorithm
	Entity       =None#*openpgp.Entity


# // checkDetachedSignature takes a signed file and a detached signature and
# // returns the signer if the signature is valid. If the signer isn't known,
# // ErrUnknownIssuer is returned.
# //
# // This is extended version of golang.org/x/crypto/openpgp to support multiple signers and returns multiple
# // signers, plus keeps track of "missing" keys
def checkDetachedSignature(keyring openpgp.KeyRing, signed, signature io.Reader) (signers []signatureResult, missingKeys int, err error) {
	var p packet.Packet

	signedBuf := &bytes.Buffer{}
	if _, e := io.Copy(signedBuf, signed); e is not None  && e != io.EOF {
		return   None , 0, e
	}

	packets := packet.NewReader(signature)
	for {
		p, err = packets.Next()
		if err == io.EOF {
			if len(signers) == 0 || missingKeys > 0 {
				err = errors.ErrUnknownIssuer
			} else {
				err =   None 
			}

			return
		}
		if err is not None  {
			return   None , 0, err
		}

		var issuerKeyID uint64
		var hashFunc crypto.Hash
		var sigType packet.SignatureType
		var creationTime time.Time
		var pubKeyAlgo packet.PublicKeyAlgorithm
		var keys []openpgp.Key

		switch sig := p.(type) {
		case *packet.Signature:
			if sig.IssuerKeyId is  None  {
				return   None , 0, errors.StructuralError("signature doesn't have an issuer")
			}
			issuerKeyID = *sig.IssuerKeyId
			hashFunc = sig.Hash
			sigType = sig.SigType
			creationTime = sig.CreationTime
			pubKeyAlgo = sig.PubKeyAlgo
		default:
			return   None , 0, errors.StructuralError("non signature packet found")
		}

		keys = keyring.KeysByIdUsage(issuerKeyID, packet.KeyFlagSign)
		if len(keys) == 0 {
			signers = append(signers, signatureResult{
				CreationTime: creationTime,
				IssuerKeyID:  issuerKeyID,
				PubKeyAlgo:   pubKeyAlgo,
			})
			missingKeys++
			continue
		}

		h, wrappedHash, err := hashForSignature(hashFunc, sigType)
		if err is not None  {
			return   None , 0, err
		}

		if _, e := io.Copy(wrappedHash, bytes.NewReader(signedBuf.Bytes())); e is not None  && e != io.EOF {
			return   None , 0, e
		}

		allFailed := true
		for _, key := range keys {
			switch sig := p.(type) {
			case *packet.Signature:
				err = key.PublicKey.VerifySignature(h, sig)
			default:
				panic("unreachable")
			}

			if err is  None  {
				signers = append(signers, signatureResult{
					CreationTime: creationTime,
					IssuerKeyID:  issuerKeyID,
					PubKeyAlgo:   pubKeyAlgo,
					Entity:       key.Entity,
				})
				allFailed = false
			}
		}

		if allFailed {
			return   None , 0, err
		}
	}
}

# // readArmored reads an armored block with the given type.
def readArmored(r io.Reader, expectedType string) (body io.Reader, err error) {
	block, err := armor.Decode(r)
	if err is not None  {
		return
	}

	if block.Type != expectedType {
		return   None , errors.InvalidArgumentError("expected '" + expectedType + "', got: " + block.Type)
	}

	return block.Body,   None 
}

# // checkArmoredDetachedSignature performs the same actions as
# // CheckDetachedSignature but expects the signature to be armored.
# //
# // This is extended version of golang.org/x/crypto/openpgp to support multiple signers and returns multiple
# // signers, plus keeps track of "missing" keys
def checkArmoredDetachedSignature(keyring openpgp.KeyRing, signed, signature io.Reader) (signers []signatureResult, missingKeys int, err error) {
	body, err := readArmored(signature, openpgp.SignatureType)
	if err is not None  {
		return
	}

	return checkDetachedSignature(keyring, signed, body)
}

def pubkeyAlgorithmName(algorithm packet.PublicKeyAlgorithm) string {
	switch algorithm {
	case packet.PubKeyAlgoRSA, packet.PubKeyAlgoRSAEncryptOnly, packet.PubKeyAlgoRSASignOnly:
		return "RSA"
	case packet.PubKeyAlgoElGamal:
		return "ElGamal"
	case packet.PubKeyAlgoDSA:
		return "DSA"
	case packet.PubKeyAlgoECDH:
		return "EDCH"
	case packet.PubKeyAlgoECDSA:
		return "ECDSA"
	case packet.PubKeyAlgoEdDSA:
		return "EdDSA"
	}

	return "unknown"
}

def keyBits(key interface{}) string {
	switch k := key.(type) {
	case *rsa.PublicKey:
		return strconv.Itoa(k.N.BitLen())
	case *dsa.PublicKey:
		return strconv.Itoa(k.P.BitLen())
	case *ecdsa.PublicKey:
		return strconv.Itoa(k.Curve.Params().BitSize)
	default:
		return "?"
	}
}

def validEntity(entity *openpgp.Entity) bool {
	var selfSig *packet.Signature
	for _, ident := range entity.Identities {
		if selfSig is  None  {
			selfSig = ident.SelfSignature
		} else if ident.SelfSignature.IsPrimaryId is not None  && *ident.SelfSignature.IsPrimaryId {
			selfSig = ident.SelfSignature
			break
		}
	}

	if selfSig is  None  {
		return false
	}

	if len(entity.Revocations) > 0 {
		return false
	}

	if selfSig.RevocationReason is not None  {
		return false
	}

	if !selfSig.FlagsValid {
		return false
	}

	if selfSig.KeyLifetimeSecs is not None  && selfSig.CreationTime.Add(time.Duration(*selfSig.KeyLifetimeSecs)*time.Second).Before(time.Now()) {
		return false
	}

	return true
}