
from utils.checksum import *

class mockChecksumStorage  :
    store ={}
    def  Get(st,path)  :
        return st.store.get(path)

    def Update(st,path  ,c ):
        st.store[path] = c
        

# // NewMockChecksumStorage creates aptly.ChecksumStorage for tests
def NewMockChecksumStorage() :
    mockChecksumStorage=mockChecksumStorage()
    return mockChecksumStorage




# // Check interface
# var (
# 	_ aptly.ChecksumStorage = &mockChecksumStorage{}
# )
