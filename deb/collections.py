# package deb

# import (
# 	"sync"

# 	"github.com/aptly-dev/aptly/aptly"
# 	"github.com/aptly-dev/aptly/database"
# )

# // CollectionFactory is a single place to generate all desired collections
import database
from deb.package_collection import *
from deb.local import *
from  deb.checksum_collection import *
from deb.publish import *
from deb.snapshot import *
from deb.publish import PublishedRepoCollection,NewPublishedRepoCollection
class CollectionFactory:
# 	*sync.Mutex
    db:database.Storage=None
    packages       :PackageCollection=None
    # remoteRepos    *RemoteRepoCollection
    snapshots      :SnapshotCollection=None
    localRepos     :LocalRepoCollection=None
    publishedRepos :PublishedRepoCollection=None
    checksums      :ChecksumCollection=None
    # // LocalRepoCollection returns (or creates) new LocalRepoCollection
    def  LocalRepoCollection(factory)->"LocalRepoCollection" :
        if factory.localRepos is None  :
            factory.localRepos = NewLocalRepoCollection(factory.db)
        return factory.localRepos
    
    # // PackageCollection returns (or creates) new PackageCollection
    def  PackageCollection(factory) ->"PackageCollection" :

        if factory.packages is None  :
            factory.packages = NewPackageCollection(factory.db)
        

        return factory.packages
    # // ChecksumCollection returns (or creates) new ChecksumCollection
    def  ChecksumCollection(factory,db :database.Storage) :

        if db is not None  :
            return NewChecksumCollection(db)
        

        if factory.checksums is None  :
            factory.checksums = NewChecksumCollection(factory.db)
        

        return factory.checksums
    
    # // PublishedRepoCollection returns (or creates) new PublishedRepoCollection
    def  PublishedRepoCollection(factory) ->"PublishedRepoCollection" :


        if factory.publishedRepos is None  :
            factory.publishedRepos = NewPublishedRepoCollection(factory.db)
        return factory.publishedRepos
    # // SnapshotCollection returns (or creates) new SnapshotCollection
    def SnapshotCollection(factory) ->SnapshotCollection :
        if factory.snapshots is None  :
            factory.snapshots = NewSnapshotCollection(factory.db)
        return factory.snapshots
    

# // NewCollectionFactory creates new factory
def NewCollectionFactory(db :database.Storage) :
    collectionFactory=CollectionFactory()
    collectionFactory.db=db
    return collectionFactory


# // TemporaryDB creates new temporary DB
# //
# // DB should be closed/droped after being used
# def (factory *CollectionFactory) TemporaryDB() (database.Storage, error) {
# 	return factory.db.CreateTemporary()
# }



# // RemoteRepoCollection returns (or creates) new RemoteRepoCollection
# def (factory *CollectionFactory) RemoteRepoCollection() *RemoteRepoCollection {
# 	
# 	

# 	if factory.remoteRepos is None  {
# 		factory.remoteRepos = NewRemoteRepoCollection(factory.db)
# 	}

# 	return factory.remoteRepos
# }








# // Flush removes all references to collections, so that memory could be reclaimed
# def (factory *CollectionFactory) Flush() {
# 	
# 	

# 	factory.localRepos = None
# 	factory.snapshots = None
# 	factory.remoteRepos = None
# 	factory.publishedRepos = None
# 	factory.packages = None
# 	factory.checksums = None
# }
