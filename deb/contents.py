# package deb

# import (
# 	"bytes"
# 	"errors"
# 	"fmt"
# 	"io"

# 	"github.com/aptly-dev/aptly/database"
# 	"github.com/pborman/uuid"
# )

# // ContentsIndex calculates mapping from files to packages, with sorting and aggregation
from uuid import uuid4
from database import database
from typing import List,Dict,Tuple
class ContentsIndex(list) :
	db     :"database.Storage"=None
	prefix =''#None



	# // Push adds package to contents index, calculating package contents as required
	def   Push(index,qualifiedName :list, contents :List[str], dbw :database.Storage) :
		for   path in contents :
			# // for performance reasons we only write to leveldb during push.
			# // merging of qualified names per path will be done in WriteTo
			contentName=index.prefix+path+'0'+''.join(qualifiedName)
			# dbw.Put(append(append(append(index.prefix, []byte(path)...), byte(0)), qualifiedName...),   None )
			dbw.Put(contentName, None)
		return None

	# // Empty checks whether index contains no packages
	def  Empty(index) ->bool :
		return not index.db.HasPrefix(index.prefix)
	

# // WriteTo dumps sorted mapping of files to qualified package names
	def WriteTo(index,w ) ->Tuple[int, str] :
		# // For performance reasons push method wrote on key per path and package
		# // in this method we now need to merge all packages which have the same path
		# // and write it to contents index file

		n=0# int

		nn = w.write("{} {}\n".format( "FILE", "LOCATION"))
		n += int(nn)
		# if err is not None  :
		# 	return n, err
		

		prefixLen = len(index.prefix)

	
		currentPath= ''#[]byte
		currentPkgs =[]#[][]byte
		
		def NoName(key,value):
			# // cut prefix
			key = key[prefixLen:]

			i = key.find('0')
			if i == -1 :
				return "corrupted index entry"
			

			path = key[:i]
			pkg = key[i+1:]

			if  path!=currentPath:
				if currentPath is not None  :
					currentPath+=' '
					nn  = w.write(currentPath)
					n += int(nn)
					# if err is not None  :
					# 	return err
					
					# nn = w.write(bytes.Join(currentPkgs, []byte{','}))
					nn = w.write(','.join(currentPkgs))
					n += int(nn)
					# if err is not None  {
					# 	return err
					# }

					nn  = w.write('\n')
					n += int(nn)
					# if err is not None  {
					# 	return err
					# }
				

				# currentPath = append([]byte(None), path...)
				currentPath = path
				currentPkgs = None
			
			currentPkgs.append(pkg)
			# currentPkgs = append(, append([]byte(None), pkg...))

			return None
		err = index.db.ProcessByPrefix(index.prefix, NoName)

		if err is not None  :
			return n, err
		

		if currentPath is not None  :
			currentPath+=' '
			nn = w.write(currentPath)
		
			n += int(nn)
			# if err is not None  :
			# 	return n, err
			
			
			# nn  = w.write(bytes.Join(currentPkgs, []byte{','}))
			nn = w.write(','.join(currentPkgs))
			n += int(nn)

			# if err is not None  :
			# 	return n, err
			

			nn = w.write('\n')
			n += int(nn)
		

		return n, err

# // NewContentsIndex creates empty ContentsIndex
def NewContentsIndex(db :database.Storage) ->ContentsIndex :
	contentsIndex=ContentsIndex()
	contentsIndex.db=db
	contentsIndex.prefix=str(uuid4())
	return contentsIndex
