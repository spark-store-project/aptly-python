import sys
SourceSnapshot   = "snapshot"
SourceLocalRepo  = "local"
SourceRemoteRepo = "repo"
if sys.platform!='linux':
    from deb.deb_common import *
else:
    from deb.deb_linux import *