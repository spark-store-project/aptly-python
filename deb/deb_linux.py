
from apt.debfile import *
from io import StringIO
from .formatp import *
def GetControlFileFromDeb(packageFile:str):
    # file=open(packageFile)
    file=DebPackage(packageFile)
    controlContent=StringIO(file.control_content('control'))
    print(controlContent.getvalue())
    reader = NewControlFileReader(controlContent, False, False)
    print(reader.__dict__)
    stanza,err=reader.ReadStanza()
    return stanza,err
# // GetControlFileFromDsc reads control file from dsc package
def GetControlFileFromDsc(dscFile , verifier) :
    file= open(dscFile)

    isClearSigned= verifier.IsClearSigned(file)
    file.Seek(0, 0)
    if isClearSigned :
        text = verifier.ExtractClearsigned(file)
    else :
        text = file
    

    reader = NewControlFileReader(text, False, False)
    stanza= reader.ReadStanza()

    return stanza

# // GetContentsFromDeb returns list of files installed by .deb package
def GetContentsFromDeb( packageFile ) :
    debInfo=DebPackage(packageFile)
    return debInfo.filelist
    
if __name__=="__main__":
    filePath=''
    a=GetContentsFromDeb(filePath)
    print(a)