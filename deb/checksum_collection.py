# package deb

# import (
# 	"bytes"

# 	"github.com/aptly-dev/aptly/aptly"
# 	"github.com/aptly-dev/aptly/database"
# 	"github.com/aptly-dev/aptly/utils"
# 	"github.com/ugorji/go/codec"
# )

# // ChecksumCollection does management of ChecksumInfo in DB
from database import database
from utils.checksum import *
import pickle
class ChecksumCollection :
	db:database.Storage=None
	# // codecHandle *codec.MsgpackHandle
	# // Update adds or updates information about checksum in DB
	def  Update(collection,path :str, c :ChecksumInfo) :
		

		return collection.db.Put(collection.dbKey(path), pickle.dumps(c))
	

	def  dbKey(collection,path :str) :
		return "C" + path
	# // Get finds checksums in DB by path
	def  Get(collection,path :str) ->ChecksumInfo:
		encoded= collection.db.Get(collection.dbKey(path))
		if encoded is None:
			return None
		c = pickle.loads(encoded)

		# decoder := codec.NewDecoderBytes(encoded, collection.codecHandle)
		# err = decoder.Decode(c)
		# if err is not None  {
		# 	return None, err
		# }

		return c
	

# // NewChecksumCollection creates new ChecksumCollection and binds it to database
def NewChecksumCollection(db :database.Storage) ->ChecksumCollection :
	checksumCollection=ChecksumCollection()
	checksumCollection.db=db
	return checksumCollection







# // Check interface
# var (
# 	_ aptly.ChecksumStorage = &ChecksumCollection{}
# )
