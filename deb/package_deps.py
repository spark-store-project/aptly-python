from typing import List,Dict
class PackageDependencies(object):
    Depends:List[str]=[]
    BuildDepends:List[str]=[]
    BuildDependsInDep:List[str]=[]
    PreDepends:List[str]=[]
    Suggests:List[str]=[]
    Recommends:List[str]=[]
    def __getstate__(self):
        state=self.__dict__.copy()
        state['BuildDepends']=self.BuildDepends
        state['Depends']=self.Depends
        state['BuildDependsInDep']=self.BuildDependsInDep
        state['PreDepends']=self.PreDepends
        state['Suggests']=self.Suggests
        state['Recommends']=self.Recommends
        return state
def parseDependencies(inputStanza:Dict[str,str],key:str)->List[str]:
    value=inputStanza.get(key)
    if not value:
        return None
    inputStanza.pop(key)
    value=value.strip()
    if value=='':
        return None
    result=value.split(',')
    for index in range(len(result)):
        result[index]=result[index].strip()
    return result