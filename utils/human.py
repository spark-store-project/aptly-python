# DONE 翻译完成
def HumanBytes(size:int):
    if size > (512 * 1024 * 1024 * 1024):
        result = "{:.2f} TiB".format(size/1024/1024/1024/1024)
    elif size > (512 * 1024 * 1024):
        result = "{:.2f} GiB".format(size/1024/1024/1024)
    elif size > (512 * 1024):
        result = "{:.2f} MiB".format(size/1024/1024)
    elif size > 512:
        result = "{:.2f} KiB".format(size/1024)
    else:
        result = "{} B".format(size)
    return result
if __name__=="__main__":
    print(HumanBytes(1024))
    