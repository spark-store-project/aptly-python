import os
import gzip
import bz2
# DONE 翻译完成
def CompressFile(source:str,onlyGzip:bool=False):
    
    gzPath=os.path.basename(source+'.gz')
    with gzip.GzipFile(os.path.join(os.path.dirname(source),gzPath),'wb') as f:
        f.write(open(source,'rb').read())
    if not onlyGzip:
        bzFileName=os.path.basename(source+'.bz2')
        with bz2.open(os.path.join(os.path.dirname(source),bzFileName),'wb') as f:
            f.write(open(source,'rb').read())
    return None
if __name__=="__main__":
    CompressFile('config.py')