import hashlib
# DONE 翻译完成
def MD5ChecksumForFile(path):
    with open(path, 'rb') as fp:
        data = fp.read()
    return hashlib.md5(data).hexdigest()

class ChecksumInfo(object):
    Size :int=0
    MD5 :str=''
    SHA1 :str=''
    SHA256 :str=""
    SHA512 :str=""


    def Complete(self)->bool:
        return self.MD5 != "" and self.SHA1 != "" and self.SHA256 != "" and self.SHA512 != ""

def ChecksumsForFile(path :str)->ChecksumInfo:
    f=open(path,'rb')
    data=f.read()
    f.close()
    w=NewChecksumWriter()
    w.Write(data)
    return w.Sum()

class ChecksumWriter(object):
    sum:ChecksumInfo=None
    hashes=[]
    def __init__(self) -> None:
        self.sum=ChecksumInfo()
    def Write(c,p:int):
        c.sum.Size+=len(p)
        # 计算
        for i in c.hashes:
            i.update(p)
        return len(p)

    def Sum(c):
        c.sum.MD5=c.hashes[0].hexdigest()
        c.sum.SHA1=c.hashes[1].hexdigest()
        c.sum.SHA256=c.hashes[2].hexdigest()
        c.sum.SHA512=c.hashes[3].hexdigest()
        return c.sum
# 返回四个计算
def NewChecksumWriter():
    checksumWriter=ChecksumWriter()
    checksumWriter.hashes=[hashlib.md5(),hashlib.sha1(),hashlib.sha256(),hashlib.sha512()]
    return checksumWriter


if __name__=="__main__":
    c=ChecksumsForFile('a.conf')
    print(c.__dict__)