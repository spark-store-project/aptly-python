import re
def containsAny(seq:str,aset:str)->bool:
    for c in seq:
        if c in aset:
            return True
    return False
def Match(pattern,val):
    return re.search(pattern, val)
def sortSearch(n:int,func):
    i, j = 0, n
    while i < j :
        h = int((i+j) >> 1) #// avoid overflow when computing h
        #// i ≤ h < j
        if not func(h) :
            i = h + 1 # // preserves f(i-1) == false
        else :
            j = h #// preserves f(j) == true
        
    
    #// i == j, f(i-1) == false, and f(j) (= f(i)) == true  =>  answer is i.
    return i