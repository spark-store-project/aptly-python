# package utils

# import (
#     "fmt"
#     "sort"
# )
from typing import List,Dict
# // StringsIsSubset checks that subset is strict subset of full, and returns
# // error formatted with errorFmt otherwise
# def StringsIsSubset(subset, full []string, errorFmt string) error {
#     for _, checked = range subset {
#         found = False
#         for _, s = range full {
#             if checked == s {
#                 found = True
#                 break
#             }
#         }
#         if !found {
#             return fmt.Errorf(errorFmt, checked)
#         }
#     }
#     return   None 
# }

# // StrSlicesEqual compares two slices for equality
# def StrSlicesEqual(s1, s2 []string) bool {
#     if len(s1) != len(s2) {
#         return False
#     }

#     for i, s = range s1 {
#         if s != s2[i] {
#             return False
#         }
#     }

#     return True
# }

# // StrMapsEqual compares two map[string]string
# def StrMapsEqual(m1, m2 map[string]string) bool {
#     if len(m1) != len(m2) {
#         return False
#     }

#     for k, v = range m1 {
#         v2, ok = m2[k]
#         if !ok || v != v2 {
#             return False
#         }
#     }

#     return True
# }

# // StrSliceHasItem checks item for presence in slice
def StrSliceHasItem(s :List[str], item :str) ->bool :
    for  v in s :
        if v == item :
            return True
        
    
    return False


# // StrMapSortedKeys returns keys of map[string]string sorted
# def StrMapSortedKeys(m map[string]string) []string {
#     keys = make([]string, len(m))
#     i = 0
#     for k = range m {
#         keys[i] = k
#         i++
#     }
#     sort.Strings(keys)
#     return keys
# }

# // StrSliceDeduplicate removes dups in slice
def StrSliceDeduplicate(s :List[str]) ->List[str] :
    l = len(s)
    if l < 2 :
        return s
    
    if l == 2 :
        if s[0] == s[1] :
            return s[0:1]
        
        return s
    

    found = {}
    j = 0
    for i in range (l) :
        if not found[s[i]] :
            found[s[i]] = True
            s[j] = s[i]
            j+=1
    return s[:j]


# // StrSlicesSubstract finds all the strings which are in l but not in r, both slices shoult be sorted
def StrSlicesSubstract(l, r :List[str])-> List[str]:
    result =[]

    # // pointer to left and right reflists
    il, ir = 0, 0
    # // length of reflists
    ll, lr = len(l), len(r)

    while il < ll or ir < lr :
        if il == ll :
            # // left list exhausted, we got the result
            break
        
        if ir == lr :
            # // right list exhausted, append what is left to result
            result=result+l[il:]
            break
        

        if l[il] == r[ir] :
            # // r contains entry from l, so we skip it
            il+=1
            ir+=1
        elif l[il] < r[ir] :
            # // item il is not in r, append
            result .append( l[il])
            il+=1
        else :
            # // skip over to next item in r
            ir+=1
        # }
    # }

    return result

