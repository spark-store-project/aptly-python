
from typing import Tuple
import os
import json
# DONE 翻译完成
FileSystemPublishRoot = {
    "RootDir":"",
    "LinkMethod"  :"hardlink",
    "VerifyMethod" :""
}
class ConfigStructure (object):
    RootDir :str=os.path.join(os.getenv("HOME"), ".aptly")
    DownloadConcurrency :int=   4
    DownloadLimit :int=         0
    DownloadRetries:int
    Downloader:str=''
    DatabaseOpenAttempts :int=   -1
    DepVerboseResolve:bool=None
    Architectures:list=         []
    DepFollowSuggests:bool=     False
    DepFollowRecommends:bool=    False
    DepFollowAllVariants:bool=   False
    DepFollowSource:bool=        False
    GpgProvider:str=           "gpg"
    GpgDisableSign:bool=        False
    GpgDisableVerify:bool=      False
    DownloadSourcePackages:bool= False
    SkipLegacyPool:bool=        False
    PpaDistributorID:str=      "ubuntu"
    PpaCodename:str=           ""
    SkipBz2Publishing  :bool=False#    bool
    SkipContentsPublishing:bool=None
    FileSystemPublishEndpoints:dict= {"":FileSystemPublishRoot}
    # AzurePublishRoots      map[string]AzurePublishRoot      `json:"AzurePublishEndpoints"`
    AsyncAPI               :bool=False #                             `json:"AsyncAPI"`
    EnableMetricsEndpoint  :bool   =False
    ServeInAPIMode         :bool=False
    def getAllAttr(self):
        return ['Architectures', 'DatabaseOpenAttempts', 'DepFollowAllVariants', 'DepFollowRecommends', 'DepFollowSource', 'DepFollowSuggests', 'DownloadConcurrency', 'DownloadLimit', 'DownloadSourcePackages', 'FileSystemPublishEndpoints', 'GpgDisableSign', 'GpgDisableVerify', 'GpgProvider', 'PpaCodename', 'PpaDistributorID', 'RootDir', 'SkipLegacyPool']
    # S3PublishRoots:         map[string]S3PublishRoot{},
    # SwiftPublishRoots:      map[string]SwiftPublishRoot{},
Config:ConfigStructure=ConfigStructure()

def LoadConfig(filename:str)->Tuple[ConfigStructure,str]:
    # DONE 此处有问题
    if not os.path.exists(filename):
        json_obj={}
        for key in Config.getAllAttr():
            json_obj[key] = getattr(Config,key)
        with open(filename,'w') as f:
            f.write(json.dumps(json_obj,  sort_keys=True, indent=4))
    try:
        with open(filename) as f:
            try:
                data=json.load(f)
            except:
                data={}
            for i in data.keys():
                setattr(Config,i,data[i])
            return Config,None
    except:
        print('配置读取失败')
        return None,'读取配置失败'
def SaveConfig(filename:str,config:ConfigStructure):
    # LoadConfig(filename)
    json_obj={}
    for key in config.getAllAttr():
        json_obj[key] = getattr(config,key)
    with open(filename,'w') as f:
        f.write(json.dumps(json_obj,  sort_keys=True, indent=4))
if __name__=="__main__":
    SaveConfig('a.conf',ConfigStructure)
    print(LoadConfig('a.conf'))