import os
# DONE 翻译完成
def DirIsAccessible(filename:str):
    try:
        os.stat(filename)
    except Exception as e:
        return "错误：文件目录 {}:[{}] {}".format(filename,e.errno,e.e.strerror)
    if not os.path.exists(filename):
        return "错误：文件目录 {}:不存在".format(filename)
    else:
        if not os.access(filename,os.W_OK):
            return "错误： '{}' 访问被拒绝，请检查".format(filename)
    return None