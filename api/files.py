# package api

# import (
#     "fmt"
#     "io"
#     "os"
#     "path/filepath"
#     "strings"

#     "github.com/gin-gonic/gin"
# )
import os
from context import context
def verifyPath(path :str) ->bool :
    # path = filepath.Clean(path)
    for part in path.split(os.sep) :
        if part == ".." or part == "." :
            return False
        
    

    return True



def verifyDir(c :dict) ->bool :
    if not verifyPath(c.get("dir")) :
        print("wrong dir")
        return False
    

    return True


# // GET /files
def apiFilesListDirs(c :dict) :
    list = []
    def noName(path string, info os.FileInfo, err error)->str:
        if err is not None  :
            return err

        if path == context.UploadPath() :
            return   None 
        

        if info.IsDir() :
            list = append(list, filepath.Base(path))
            return filepath.SkipDir
        

        return   None 
    err = filepath.Walk(context.UploadPath(), noName)

    # if err is not None  and not os.IsNotExist(err) :
    #     c.AbortWithError(400, err)
    #     return
    

    print(200,list)


# // POST /files/:dir/
# def apiFilesUpload(c :dict) {
# 	if !verifyDir(c) {
# 		return
# 	}

# 	path := filepath.Join(context.UploadPath(), c.Params.ByName("dir"))
# 	err := os.MkdirAll(path, 0777)

# 	if err is not None  {
# 		AbortWithJSONError(c, 500, err)
# 		return
# 	}

# 	err = c.Request.ParseMultipartForm(10 * 1024 * 1024)
# 	if err is not None  {
# 		AbortWithJSONError(c, 400, err)
# 		return
# 	}

# 	stored := []string{}

# 	for _, files := range c.Request.MultipartForm.File {
# 		for _, file := range files {
# 			src, err := file.Open()
# 			if err is not None  {
# 				AbortWithJSONError(c, 500, err)
# 				return
# 			}
# 			defer src.Close()

# 			destPath := filepath.Join(path, filepath.Base(file.Filename))
# 			dst, err := os.Create(destPath)
# 			if err is not None  {
# 				AbortWithJSONError(c, 500, err)
# 				return
# 			}
# 			defer dst.Close()

# 			_, err = io.Copy(dst, src)
# 			if err is not None  {
# 				AbortWithJSONError(c, 500, err)
# 				return
# 			}

# 			stored = append(stored, filepath.Join(c.Params.ByName("dir"), filepath.Base(file.Filename)))
# 		}
# 	}

# 	apiFilesUploadedCounter.WithLabelValues(c.Params.ByName("dir")).Inc()
# 	c.JSON(200, stored)

# }

# // GET /files/:dir
# def apiFilesListFiles(c :dict) {
# 	if !verifyDir(c) {
# 		return
# 	}

# 	list := []string{}
# 	root := filepath.Join(context.UploadPath(), c.Params.ByName("dir"))

# 	err := filepath.Walk(root, func(path string, _ os.FileInfo, err error) error {
# 		if err is not None  {
# 			return err
# 		}

# 		if path == root {
# 			return   None 
# 		}

# 		list = append(list, filepath.Base(path))

# 		return   None 
# 	})

# 	if err is not None  {
# 		if os.IsNotExist(err) {
# 			AbortWithJSONError(c, 404, err)
# 		} else {
# 			AbortWithJSONError(c, 500, err)
# 		}
# 		return
# 	}

# 	c.JSON(200, list)
# }


# // DELETE /files/:dir
# def apiFilesDeleteDir(c :dict) {
#     if !verifyDir(c) {
#         return
#     }

#     err := os.RemoveAll(filepath.Join(context.UploadPath(), c.Params.ByName("dir")))
#     if err is not None  {
#         c.AbortWithError(500, err)
#         return
#     }

#     c.JSON(200, gin.H{})
# }

# // DELETE /files/:dir/:name
# def apiFilesDeleteFile(c :dict) {
#     if !verifyDir(c) {
#         return
#     }

#     if !verifyPath(c.Params.ByName("name")) {
#         c.AbortWithError(400, fmt.Errorf("wrong file"))
#         return
#     }

#     err := os.Remove(filepath.Join(context.UploadPath(), c.Params.ByName("dir"), c.Params.ByName("name")))
#     if err is not None  {
#         if err1, ok := err.(*os.PathError); !ok || !os.IsNotExist(err1.Err) {
#             c.AbortWithError(500, err)
#             return
#         }
#     }

#     c.JSON(200, gin.H{})
# }
